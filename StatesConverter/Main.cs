﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using StatesConverter.Logic;

namespace StatesConverter
{
	public partial class Main : Form
	{
		public Main()
		{
			this.InitializeComponent();
		}


		private void selectFileButton_Click(
			object sender,
			EventArgs e)
		{
			this.openFileDialog1.ShowDialog();
		}


		private void saveFileButton_Click(
			object sender,
			EventArgs e)
		{
			this.saveFileDialog1.ShowDialog();
		}


		private void openFileDialog1_FileOk(
			object sender,
			CancelEventArgs e)
		{
			this.openFileTextBox.Text = this.openFileDialog1.FileName;
		}


		private void saveFileDialog1_FileOk(
			object sender,
			CancelEventArgs e)
		{
			this.saveFileTextBox.Text = this.saveFileDialog1.FileName;
		}


		private void processButton_Click(
			object sender,
			EventArgs e)
		{
			StatesConfigProcessor processor = new StatesConfigProcessor(this.openFileDialog1.OpenFile(),
			                                                            this.saveFileDialog1.FileName);
			this.messageLabel.Text = processor.ProcessStatesConfig();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.openFileDialog2.ShowDialog();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.saveFileDialog2.ShowDialog();
		}

		private void saveFileDialog2_FileOk(object sender, CancelEventArgs e)
		{
			this.textBox1.Text = this.saveFileDialog2.FileName;
		}

		private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
		{
			this.textBox2.Text = this.openFileDialog2.FileName;
		}

		private void generateButton_Click(object sender, EventArgs e)
		{
			XGmlProcessor processor = new XGmlProcessor(this.openFileDialog2.OpenFile(), this.saveFileDialog2.FileName);
			processor.StartProcessing();
		}

	}
}