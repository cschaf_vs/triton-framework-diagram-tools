﻿namespace StatesConverter
{
	public static class NodeConstants
	{
		public const string ACTION_ALIGN = "left";
		public const string ACTION_ANCHOR = "c";
		public const string ACTION_COLOR = "#FFFFFF";
		public const string ACTION_FILL = "#18EC12";
		public const string ACTION_FONT_NAME = "Dialog";
		public const string ACTION_FONT_SIZE = "12";
		public const string ACTION_H = "120";
		public const string ACTION_HAS_OUTLINE = "false";
		public const string ACTION_SHAPE = "roundrectangle";
		public const string ACTION_X = "10";
		public const string ACTION_Y = "10";

		public const string EDGE_FILL = "#000000";
		public const string EDGE_FONT_NAME = "Dialog";
		public const string EDGE_FONT_SIZE = "12";
		public const string EDGE_MODEL = "six_pos";
		public const string EDGE_POSITION = "tail";
		public const string EDGE_STYLE = "dashed";
		public const string EDGE_TARGET_ARROW = "standard";

		public const string ELLIPSE_ALIGN = "left";
		public const string ELLIPSE_ANCHOR = "c";
		public const string ELLIPSE_COLOR = "#FFFFFF";
		public const string ELLIPSE_FILL = "#336611";
		public const string ELLIPSE_FONT_NAME = "Dialog";
		public const string ELLIPSE_FONT_SIZE = "12";
		public const string ELLIPSE_H = "120";
		public const string ELLIPSE_HAS_OUTLINE = "false";
		public const string ELLIPSE_SHAPE = "ellipse";
		public const string ELLIPSE_X = "10";
		public const string ELLIPSE_Y = "10";

		public const string PAGE_ALIGN = "left";
		public const string PAGE_ANCHOR = "c";
		public const string PAGE_COLOR = "#FFFFFF";
		public const string PAGE_FILL = "#3366FF";
		public const string PAGE_FONT_NAME = "Dialog";
		public const string PAGE_FONT_SIZE = "12";
		public const string PAGE_H = "120";
		public const string PAGE_HAS_OUTLINE = "false";
		public const string PAGE_SHAPE = "rectangle";
		public const string PAGE_X = "10";
		public const string PAGE_Y = "10";

		public const string DIAMOND_ALIGN = "left";
		public const string DIAMOND_ANCHOR = "c";
		public const string DIAMOND_COLOR = "#FFFFFF";
		public const string DIAMOND_FILL = "#3366FF";
		public const string DIAMOND_FONT_NAME = "Dialog";
		public const string DIAMOND_FONT_SIZE = "12";
		public const string DIAMOND_H = "120";
		public const string DIAMOND_HAS_OUTLINE = "false";
		public const string DIAMOND_SHAPE = "diamond";
		public const string DIAMOND_X = "10";
		public const string DIAMOND_Y = "10";

		public const string GROUP_ALIGN = "left";
		public const string GROUP_ANCHOR = "c";
		public const string GROUP_COLOR = "#FFFFFF";
		public const string GROUP_FILL = "#3366FF";
		public const string GROUP_FONT_NAME = "Dialog";
		public const string GROUP_FONT_SIZE = "12";
		public const string GROUP_H = "120";
		public const string GROUP_HAS_OUTLINE = "false";
		public const string GROUP_SHAPE = "parallelogram";
		public const string GROUP_X = "10";
		public const string GROUP_Y = "10";

		public const string OCTAGON_ALIGN = "left";
		public const string OCTAGON_ANCHOR = "c";
		public const string OCTAGON_COLOR = "#FFFFFF";
		public const string OCTAGON_FILL = "#FF0000";
		public const string OCTAGON_FONT_NAME = "Dialog";
		public const string OCTAGON_FONT_SIZE = "12";
		public const string OCTAGON_H = "120";
		public const string OCTAGON_HAS_OUTLINE = "false";
		public const string OCTAGON_SHAPE = "octagon";
		public const string OCTAGON_X = "10";
		public const string OCTAGON_Y = "10";

		public const string XGML_CREATOR = "yFiles";
		public const string XGML_DIRECTED = "1";
		public const string XGML_HIERARCHIC = "1";
		public const string XGML_VERSION = "2.6";
	}
}